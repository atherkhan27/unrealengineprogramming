// Fill out your copyright notice in the Description page of Project Settings.

#include "PersonalProject.h"
#include "Door.h"
#include "MyCharacter.h"


// Sets default values for this component's properties
UDoor::UDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDoor::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	OpenAngle();
	// ...
}

void UDoor::OpenAngle()
{
	AMyCharacter* character = Cast<AMyCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	if (character->GetbDoor()) {
		GEngine->AddOnScreenDebugMessage(0, 15.f, FColor::Black, "door is now open");
		GetOwner()->SetActorRotation(FRotator(0.f, 150.f, 0.f));
	}
}
