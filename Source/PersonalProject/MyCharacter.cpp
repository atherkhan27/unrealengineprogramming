// Fill out your copyright notice in the Description page of Project Settings.

#include "PersonalProject.h"
#include "MyCharacter.h"


// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("MoveForward", this, &AMyCharacter::MoveRight);
	InputComponent->BindAxis("MoveRight", this, &AMyCharacter::MoveForward);
	InputComponent->BindAction("OpenDoor", IE_Pressed, this, &AMyCharacter::OpenDoor);
}

void AMyCharacter::MoveForward(float amount)
{
	if (Controller && amount) {
		AddMovementInput(GetActorForwardVector(), amount);
	}
}

void AMyCharacter::MoveRight(float amount)
{
	if (Controller && amount) {
		AddMovementInput(GetActorRightVector(), amount);
	}
}

void AMyCharacter::OpenDoor()
{
	IsbTrue(true);
	if (GetbDoor()) {
		GEngine->AddOnScreenDebugMessage(0, 15.f, FColor::Black, "The door is open!");
	}
}

bool AMyCharacter::GetbDoor()
{
	return this->bDoor;
}

void AMyCharacter::IsbTrue(bool bDoor)
{
	this->bDoor = bDoor;
}

